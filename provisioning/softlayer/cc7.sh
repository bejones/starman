#!/bin/bash

# Install facter
yum -y install https://straylen.web.cern.ch/straylen/starman/repos/ai7-stable/x86_64/os/facter-2.4.3-1.cern5.ai7.x86_64.rpm

# Install epel-release and puppet
yum -y install epel-release
yum -y install --enablerepo=epel https://straylen.web.cern.ch/straylen/starman/repos/ai7-stable/x86_64/os/puppet-3.7.5cern3-1.ai7.noarch.rpm

# Install centos-release
rpm -ivh --replacefiles http://linuxsoft.cern.ch/cern/centos/7/cern/x86_64/Packages/centos-release-7-2.1511.el7.cern.2.10.1.x86_64.rpm

cat > /etc/puppet/puppet.conf << EOF
[main]
vardir                    = /var/lib/puppet
ssldir                    = /var/lib/puppet/ssl
rundir                    = /var/run/puppet
pluginsync                = true

[agent]
server                    = haptest.cern.ch
ca_server                 = funnyca.cern.ch
masterport                = 9170
ca_port                   = 9171
stringify_facts           = false
configtimeout             = 500
report                    = true
environment               = starman
runinterval               = 600
certificate_revocation    = false
dynamicfacts              = memorysize,memoryfree,swapsize,swapfree,uptime_seconds,uptime_hours
ignoreschedules           = true
EOF

puppet agent -tv
