#!/bin/bash

# Install facter
yum -y install https://straylen.web.cern.ch/straylen/starman/repos/ai6-stable/x86_64/os/facter-2.4.3-1.cern5.ai6.x86_64.rpm

# Install epel-release and puppet
yum -y install epel-release
yum -y install --enablerepo=epel https://straylen.web.cern.ch/straylen/starman/repos/ai6-stable/x86_64/os/puppet-3.7.5cern3-1.ai6.noarch.rpm

# Migrate to SLC 6
rpm -ivh http://ftp.scientificlinux.org/linux/scientific/6x/x86_64/os/Packages/yum-conf-sl6x-1-2.noarch.rpm
rpm -ivh http://linuxsoft.cern.ch/cern/slc6X/x86_64/yum/os/Packages/sl-release-6.7-1.slc6.x86_64.rpm --replacefiles

sed -i -- 's/centos-release/sl-release/g' /etc/yum.conf

yum erase centos-release
yum clean all



cat > /etc/puppet/puppet.conf << EOF
[main]
vardir                    = /var/lib/puppet
ssldir                    = /var/lib/puppet/ssl
rundir                    = /var/run/puppet
pluginsync                = true

[agent]
server                    = haptest.cern.ch
ca_server                 = funnyca.cern.ch
masterport                = 9170
ca_port                   = 9171
stringify_facts           = false
configtimeout             = 500
report                    = true
environment               = starman
runinterval               = 600
certificate_revocation    = false
dynamicfacts              = memorysize,memoryfree,swapsize,swapfree,uptime_seconds,uptime_hours
ignoreschedules           = true
EOF

puppet agent -tv --tags osrepos
yum -y reinstall pam openssl
yum distro-sync
/sbin/shutdown -r now

